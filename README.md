# input_to_args

This is intended as a plugin so AFL can fuzz arguments of programs.

As such it will filter `<` and `>` from the input unless they are escaped `\>` or insinde a string `"a <> b"`.

## Testing

```bash
echo -e ".\ntarget\nbli" | cargo run -- ls
```

```bash
echo -e ".\n\"target with space\"" | cargo run -- ls
```

## Escaping

```bash
echo "a <> b" | cargo run echo
a b
```
```bash
echo '"a <> b"' | cargo run echo
a <> b
```
```bash
echo "a \<> b" | cargo run echo
a < b
```

## Build for AFL and AFL++

```bash
cargo build --release --features afl
cargo build --features afl
```
