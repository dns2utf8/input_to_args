use std::io::prelude::*;
use std::{
    env, io,
    process::{exit, Command, Stdio},
};

const NO_COMMAND_MSG: &str = "needs at least one argument as the command to run";

fn main() -> io::Result<()> {
    let stdin = io::stdin();
    let handle = stdin.lock();

    let base_args = env::args().skip(1).collect::<Vec<_>>();
    if base_args.is_empty() {
        eprintln!("{}", NO_COMMAND_MSG);
        exit(254);
    }

    for line in handle.lines() {
        let line = line?;
        exec(&base_args, &line)?;
        //std::thread::sleep(std::time::Duration::from_secs(5));
    }

    Ok(())
}

fn exec(base_args: &Vec<String>, dynamic_args: &String) -> io::Result<()> {
    let mut command = Command::new(base_args.get(0).expect(NO_COMMAND_MSG));
    command
        .args(&base_args[1..])
        //.args(dynamic_args.split(" "))
        .args(parse_dynamic_args(dynamic_args));

    let output = command.stdin(Stdio::null()).output()?;

    io::stdout().write_all(&output.stdout).unwrap();
    io::stderr().write_all(&output.stderr).unwrap();

    match output.status.code() {
        None => {
            /* Child killed by signal */
            exit(255);
        }
        Some(code) if code == 0 => {
            /* all good, continue */

            Ok(())
        }
        Some(code) => {
            /* failed with code */
            exit(code)
        }
    }
}

/// Supresses `<` and `>` unless they are escaped by `\\`
fn parse_dynamic_args<S: AsRef<str>>(input: S) -> Vec<String> {
    use EscapeMode::*;

    let input = input.as_ref();
    let mut v = Vec::new();
    let mut buffer = MaybeString::None;
    let mut escape_mode = Nothing;
    let mut ignore_current_char = false;

    for c in input.chars() {
        match c {
            '\\' if escape_mode == Nothing => {
                escape_mode = NextChar;
                ignore_current_char = true;
            }
            '<' | '>' if escape_mode == Nothing => {
                ignore_current_char = true;
            }
            '<' | '>' | '\\' if escape_mode == NextChar => {
                escape_mode = Nothing;
            }
            _ if escape_mode == NextChar => {
                eprintln!("unexpected escape: {:?}", c);
                escape_mode = Nothing;
            }
            ' ' if escape_mode == Nothing => {
                buffer.append_to(&mut v);
                ignore_current_char = true;
            }
            '"' | '\'' | '\\' => {
                match escape_mode {
                    Nothing => {
                        escape_mode = Longer(c);
                        ignore_current_char = true;
                    }
                    Longer(ec) if ec == c => {
                        buffer.append_to(&mut v);
                        escape_mode = Nothing;
                        ignore_current_char = true;
                    }
                    NextChar | Longer(_) => { /* just collect normal and escaped chars */ }
                }
            }
            _ => { /* nothing */ }
        }

        if ignore_current_char {
            ignore_current_char = false;
        } else {
            buffer.push(c)
        }
    }

    buffer.append_to(&mut v);

    v
}

#[derive(Debug, PartialEq, Eq)]
enum EscapeMode {
    Nothing,
    NextChar,
    Longer(char),
}

#[derive(Debug, PartialEq, Eq)]
enum MaybeString {
    None,
    Some(String),
}
impl MaybeString {
    fn push(&mut self, c: char) {
        match self {
            MaybeString::None => *self = MaybeString::Some(String::from(c)),
            MaybeString::Some(s) => s.push(c),
        }
    }
    fn append_to(&mut self, v: &mut Vec<String>) {
        let mut old = MaybeString::None;

        std::mem::swap(&mut old, self);

        match old {
            MaybeString::None => { /* nothing */ }
            MaybeString::Some(b) => {
                v.push(b);
            }
        }
    }
}

#[cfg(test)]
mod test_parser {
    use super::*;

    #[test]
    fn escaped() {
        let input = "\"target with space\"";
        let expected = vec!["target with space"];

        assert_eq!(expected, parse_dynamic_args(&input))
    }
    #[test]
    fn single_quote() {
        let input = "'target with space'";
        let expected = vec!["target with space"];

        assert_eq!(expected, parse_dynamic_args(&input))
    }
    #[test]
    fn escaped_mixed() {
        let input = "\"target with space\" and other stuff";
        let expected = vec!["target with space", "and", "other", "stuff"];

        assert_eq!(expected, parse_dynamic_args(&input))
    }
    #[test]
    fn empty() {
        let input = "";
        let expected: Vec<&str> = vec![];

        assert_eq!(expected, parse_dynamic_args(&input))
    }
    #[test]
    fn simple() {
        let input = "Hello, World!";
        let expected = vec!["Hello,", "World!"];

        assert_eq!(expected, parse_dynamic_args(&input))
    }
    #[test]
    fn single_escape() {
        let input = "Hello,\\ World!";
        let expected = vec!["Hello, World!"];

        assert_eq!(expected, parse_dynamic_args(&input))
    }

    #[test]
    fn output_redirect_filter() {
        let input = "Hello,> World!";
        let expected = vec!["Hello,", "World!"];

        assert_eq!(expected, parse_dynamic_args(&input), "input: {:?}", input)
    }
    #[test]
    fn output_redirect_filter_quoted() {
        let input = "'Hello,> World!'";
        let expected = vec!["Hello,> World!"];

        assert_eq!(expected, parse_dynamic_args(&input), "input: {:?}", input)
    }
    #[test]
    fn input_redirect_filter() {
        let input = "Hello,< World!";
        let expected = vec!["Hello,", "World!"];

        assert_eq!(expected, parse_dynamic_args(&input), "input: {:?}", input)
    }
    #[test]
    fn input_redirect_filter_quoted() {
        let input = "'Hello,< World!'";
        let expected = vec!["Hello,< World!"];

        assert_eq!(expected, parse_dynamic_args(&input), "input: {:?}", input)
    }

    #[test]
    fn backslash_filter() {
        let input = "Hello, \\\\ World!";
        let expected = vec!["Hello,", "\\", "World!"];

        assert_eq!(expected, parse_dynamic_args(&input))
    }

    #[test]
    fn backslash_quoted() {
        let input = "'a \\ b!'";
        let expected = vec!["a \\ b!"];

        assert_eq!(expected, parse_dynamic_args(&input), "input: {:?}", input)
    }
}
